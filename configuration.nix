{ config, options, pkgs, lib, ... }:
{
  imports =
    [
      ./hardware-configuration.nix
    ];
  system = {
    stateVersion = "22.11";
  };
  boot = {
    loader = {
      systemd-boot.enable = true;
      efi.canTouchEfiVariables = true;
    };
    kernel.sysctl = {
      "net.ipv4.ip_forward" = 1;
      "net.ipv6.conf.all.forwarding" = 1;
    };
    initrd.luks.devices = {
      root = {
        device = "/dev/disk/by-uuid/ecace02c-04ee-471a-9d3a-f841d1439304";
        preLVM = true;
        allowDiscards = true;
      };
    };
  };
  i18n = {
    supportedLocales = [
      "en_US.UTF-8/UTF-8"
      "en_DK.UTF-8/UTF-8"
    ];
    defaultLocale = "en_DK.UTF-8";
    extraLocaleSettings = {
      LC_MEASUREMENT = "en_DK.UTF-8";
      LC_TIME = "en_DK.UTF-8";
    };
  };
  time = {
    timeZone = "Europe/Berlin";
  };
  networking = {
    hostName = "fox";
    firewall = {
      allowedTCPPorts = [22 80 443];
      allowedUDPPorts = [ config.networking.wireguard.interfaces.wg1.listenPort ];
    };
    wireguard.interfaces = {
      wg1 = {
        ips = [ "192.168.16.4/24" ];
        listenPort = 51820;
        privateKeyFile = "/etc/wireguard/wg1-private";
        peers = [
          { # INET Admin VPN
            publicKey = "EF2ja8ZCZpOGLrFGysIoV8mxuUZRwHFixUCfCqqyaio=";
            allowedIPs = [ "192.168.16.0/24" "192.168.200.0/24" "192.168.201.0/24" "130.149.117.0/24" "130.149.221.0/24" "130.149.152.0/24" ];
            endpoint = "130.149.220.47:${toString config.networking.wireguard.interfaces.wg1.listenPort}";
            persistentKeepalive = 25;
          }
        ];
      };
    };
  };
  environment = {
    systemPackages = with pkgs; [
      ansible
      cmake
      dig
      emacs
      file
      firefox
      gimp
      git
      gnumake
      gparted
      gzip
      libreoffice
      pciutils
      remmina
      thunderbird
      unzip
    ];
  };
  hardware = {
  };
  security = {
    rtkit.enable = true;
  };
  services = {
    pipewire = {
      enable = true;
      alsa.enable = true;
      alsa.support32Bit = true;
      pulse.enable = true;
    };
    printing = {
      enable = true;
      drivers = with pkgs; [
        hplip
      ];
    };
    openssh = {
      enable = true;
      settings = {
        PasswordAuthentication = false;
        KbdInteractiveAuthentication = false;
      };
    };
    xserver = {
      enable = true;
      displayManager = {
        lightdm = {
          enable = true;
          greeters.gtk = {
            enable = true;
            extraConfig =
              ''
        user-background = false
        '';
          };
        };
      };
      windowManager = {
        xmonad = {
          enable = true;
          enableContribAndExtras = true;
        };
      };
      desktopManager = {
        plasma5 = {
          enable = true;
        };
        xfce = {
          enable = true;
        };
      };
    };
  };
  nix = {
  };
  users = {
    users = {
      berber = {
        uid = 1000;
        isNormalUser = true;
        extraGroups = [
          "wheel"
        ];
        openssh.authorizedKeys.keys = [
          "ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAACAQDJwzgy2DIbzDj46157xoYnyXtxMRgCJnQI5BElG5JWyshYGcFUpIorvGKL92OFO/N7jLUQ+GhGYcjAG8nEd9L/DaOZOlgLeKUnKzOKbtpOxqw2z5ftdM6GZPbG4iAV7BmFNeIrSG++zV8G8PGBtxxZ05l037PqzfqTQ/fMICX5xW4jkI2oB6KGinnk6pQB8ZTO0oFnGHS17ZLquGUQRno283BP5u6QTLmeCd1MWWTRa6eDz09bs+0OzUVbA6bp6InUACJ/SvbA6Pebs5OSOqLRy/wQIeyXUNtDUqTi6EayMI/cu2W3/qAioDyYnb+nQCk01oZoxVIsxTChEf+8UJKO0ACngoiDjLpA/iO1LZENFK7dyxuif7YoVuWbb46/DMQsgA+UCKvPnwFBVuaPGj5AsIMidUlHQFI0L6S7mMFVESVKVvyjcaghpbpwNQoJZH3thgDl2v+Z8sgEUW8ECzbLJPDr4WQcm8+bjjQO1gbPlrP32y8l5v3BbV9QBdy/GZfB7nzIpH9mf7kLzuxeVLq4+0EO1fKKdsNjdB/yL6O/7PzoNAtwEoSJ/LaxmEqtnBUwrQHlRi7wKCdCYwM7M0wGYdEQHO/eFEd6ObQM/x6+LtGeKT6nNMwv5ix4b77MYxeNhzZ3rAVazXBfPtRd+Zfgt41AvSl3u7qtu5MYx3bffQ== cardno:15 597 970"
          "ssh-ed25519 AAAAC3NzaC1lZDI1NTE5AAAAIGL4SqRZxHe+zziJ/8Q8nFfav5VfYj1zQUxnrHtrI2N4 berber@schlepptop"
        ];
      };
      testuser = {
        uid = 2000;
        isNormalUser = true;
        extraGroups = [
          "wheel"
        ];
      };
    };
  };
}
